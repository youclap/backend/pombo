import FluentMySQL
import Vapor
import YouClap

enum NotificationModelError: Error {
    case unknownNotificationType(String)
}

    enum Notification {
        case addedToGroup(addedByProfileID: Profile.ID, groupID: UnsignedBigInt)
        case challengePrivateForGroup(challengeID: UnsignedBigInt, creatorID: Profile.ID, groupID: UnsignedBigInt)
        case challengePrivateForProfile(challengeID: UnsignedBigInt, creatorID: Profile.ID)
        case challengePublic(challengeID: UnsignedBigInt, creatorID: Profile.ID)
        case notificationFollowing(followerID: Profile.ID)
    }

extension Notification {
    var kind: String {
        switch self {
        case .addedToGroup: return "PROFILE_ADDED_TO_GROUP"
        case .challengePrivateForGroup: return "NEW_CHALLENGE_PRIVATE_GROUP"
        case .challengePrivateForProfile: return "NEW_CHALLENGE_PRIVATE_PROFILE"
        case .challengePublic: return "NEW_CHALLENGE_PUBLIC"
        case .notificationFollowing: return "FOLLOWING"
        }
    }
}

extension Notification {
    typealias ID = UnsignedBigInt
}

extension Notification: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case profileID
        case typeID
        case createdDate

        case notificationID
        case addedByProfileID
        case groupID

        case challengeID
        case creatorID

        case followerID
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let kind = try container.decode(String.self, forKey: .typeID)

        switch kind {
        case "PROFILE_ADDED_TO_GROUP":
            let profileID = try container.decode(Profile.ID.self, forKey: .addedByProfileID)
            let groupID = try container.decode(UnsignedBigInt.self, forKey: .groupID)
            self = Notification.addedToGroup(addedByProfileID: profileID, groupID: groupID)

        case "NEW_CHALLENGE_PRIVATE_GROUP":
            let challengeID = try container.decode(UnsignedBigInt.self, forKey: .challengeID)
            let profileID = try container.decode(Profile.ID.self, forKey: .creatorID)
            let groupID = try container.decode(UnsignedBigInt.self, forKey: .groupID)
            self = Notification.challengePrivateForGroup(
                challengeID: challengeID, creatorID: profileID, groupID: groupID)

        case "NEW_CHALLENGE_PRIVATE_PROFILE":
            let challengeID = try container.decode(UnsignedBigInt.self, forKey: .challengeID)
            let profileID = try container.decode(Profile.ID.self, forKey: .creatorID)
            self = Notification.challengePrivateForProfile(challengeID: challengeID, creatorID: profileID)

        case "NEW_CHALLENGE_PUBLIC":
            let challengeID = try container.decode(UnsignedBigInt.self, forKey: .challengeID)
            let profileID = try container.decode(Profile.ID.self, forKey: .creatorID)
            self = Notification.challengePublic(challengeID: challengeID, creatorID: profileID)

        case "FOLLOWING":
            let profileID = try container.decode(UnsignedBigInt.self, forKey: .followerID)
            self = Notification.notificationFollowing(followerID: UnsignedBigInt(profileID))
        default:
            throw NotificationModelError.unknownNotificationType(kind)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(kind, forKey: CodingKeys.typeID)

        switch self {

        case let .addedToGroup(addedByProfileID: profileID, groupID: groupID):
            try container.encode(profileID, forKey: CodingKeys.addedByProfileID)
            try container.encode(groupID, forKey: CodingKeys.groupID)

        case let .challengePrivateForGroup(challengeID: challengeID, creatorID: profileID, groupID: groupID):
            try container.encode(challengeID, forKey: CodingKeys.challengeID)
            try container.encode(profileID, forKey: CodingKeys.addedByProfileID)
            try container.encode(groupID, forKey: CodingKeys.groupID)

        case let .challengePrivateForProfile(challengeID, creatorID):
            try container.encode(challengeID, forKey: CodingKeys.challengeID)
            try container.encode(creatorID, forKey: CodingKeys.addedByProfileID)

        case let .challengePublic(challengeID, creatorID):
            try container.encode(challengeID, forKey: CodingKeys.challengeID)
            try container.encode(creatorID, forKey: CodingKeys.addedByProfileID)

        case let .notificationFollowing(followerID):
            try container.encode(followerID, forKey: CodingKeys.followerID)

        }
    }
}

extension Notification: Content { }
