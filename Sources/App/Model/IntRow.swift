import Vapor

struct IntRow<Value: FixedWidthInteger & Content>: Content {
    let value: Value
}
