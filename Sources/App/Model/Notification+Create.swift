import FluentMySQL
import Vapor
import YouClap

extension Notification {
    struct Create {
        var profileID: Profile.ID
        var notificationType: NotificationType
    }
}

extension Notification.Create: Content {
    enum Key: CodingKey {
        case profileID
        case type
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        self.profileID = try container.decode(Profile.ID.self, forKey: .profileID)

        let typeAsString = try container.decode(String.self, forKey: .type)
        guard let notificationType = NotificationType(name: typeAsString) else {
            throw CodingError.unknownValue
        }
        self.notificationType = notificationType
    }
}
