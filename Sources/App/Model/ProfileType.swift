import FluentMySQL
import Vapor

struct ProfileType {
    var id: Int?
    var name: String
}

extension ProfileType {
    static let user = ProfileType(id: 0, name: "USER")
    static let page = ProfileType(id: 1, name: "PAGE")

    init?(name: String) {
        switch name.uppercased() {
        case ProfileType.page.name:
            self = .page
        case ProfileType.user.name:
            self = .user
        default:
            assertionFailure("💥 value \(String(describing: name.uppercased())) not handled.")
            return nil
        }
    }

    init?(id: Int) {
        switch id {
        case ProfileType.page.id:
            self = .page
        case ProfileType.user.id:
            self = .user
        default:
            assertionFailure("💥 value \(String(describing: id)) not handled.")
            return nil
        }
    }
}

extension ProfileType: MySQLModel {}
