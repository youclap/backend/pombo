import FluentMySQL
import Vapor

struct NotificationType {
    var id: Int?
    var name: String
}

extension NotificationType {
    static let following = NotificationType(id: 0, name: "FOLLOWING")
    static let profileAddedToGroup = NotificationType(id: 1, name: "PROFILE_ADDED_TO_GROUP")
    static let newChallengePublic = NotificationType(id: 2, name: "NEW_CHALLENGE_PUBLIC")
    static let newChallengePrivateProfile = NotificationType(id: 3, name: "NEW_CHALLENGE_PRIVATE_PROFILE")
    static let newChallengePrivateGroup = NotificationType(id: 4, name: "NEW_CHALLENGE_PRIVATE_GROUP")

    init?(name: String) {
        switch name.uppercased() {
        case NotificationType.following.name:
            self = .following
        case NotificationType.profileAddedToGroup.name:
            self = .profileAddedToGroup
        case NotificationType.newChallengePublic.name:
            self = .newChallengePublic
        case NotificationType.newChallengePrivateProfile.name:
            self = .newChallengePrivateProfile
        case NotificationType.newChallengePrivateGroup.name:
            self = .newChallengePrivateGroup
        default:
            assertionFailure("💥 value \(String(describing: name.uppercased())) not handled in init notification type.")
            return nil
        }
    }

    init?(id: Int) {
        switch id {
        case NotificationType.following.id:
            self = .following
        case NotificationType.profileAddedToGroup.id:
            self = .profileAddedToGroup
        case NotificationType.newChallengePublic.id:
            self = .newChallengePublic
        case NotificationType.newChallengePrivateProfile.id:
            self = .newChallengePrivateProfile
        case NotificationType.newChallengePrivateGroup.id:
            self = .newChallengePrivateGroup
        default:
            assertionFailure("💥 value \(String(describing: id)) not handled in init notification type.")
            return nil
        }
    }
}

extension NotificationType: MySQLModel {}
