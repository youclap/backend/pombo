import FluentMySQL
import Vapor
import YouClap

struct Profile {
    var id: UnsignedBigInt?
    var name: String
    var username: String
    var typeID: Int
    var avatarID: Int
    var createdDate: Date?
    var deleted: Bool

    init(id: UnsignedBigInt? = nil,
         name: String,
         username: String,
         typeID: Int = ProfileType.user.id!, //swiftlint:disable:this force_unwrapping
         avatarID: Int = AvatarType.none.id!, //swiftlint:disable:this force_unwrapping
         createdDate: Date? = nil,
         deleted: Bool = false) {
        self.id = id
        self.name = name
        self.username = username
        self.typeID = typeID
        self.avatarID = avatarID
        self.createdDate = createdDate
        self.deleted = deleted
    }
}

extension Profile: MySQLUnsignedBigIntModel {
    static let createdAtKey: TimestampKey? = \.createdDate
}

extension Profile: Content {}
