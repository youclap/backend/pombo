import FluentMySQL
import GoogleCloudKit
import GoogleCloudPubSubKit
import Vapor
import YouClap

/// Called before your application initializes.
public func configure(_ env: Environment) throws -> (Config, Services) {
    let config = Config.default()
    var services = Services.default()

    services.register(contentConfig())

    try services.register(FluentMySQLProvider())

    try services.register(GoogleCloudPubSubProvider())

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config

    middlewares.use(CORSMiddleware(configuration: corsConfiguration()))

    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    services.register { container -> DatabasesConfig in
        let logger = try container.make(Logger.self)

        guard let mysqlDatabaseConfig = MySQLDatabaseConfig.loadFromEnvironment() else {
            throw ConfigurationError.database("💥 missing required arguments")
        }

        logger.info("database configuration: \(mysqlDatabaseConfig)")

        var databaseConfig = DatabasesConfig()
        databaseConfig.enableLogging(on: .mysql)
        databaseConfig.add(database: MySQLDatabase(config: mysqlDatabaseConfig), as: .mysql)

        return databaseConfig
    }

    guard let elasticSearchConfig = ElasticSearchConfig.loadFromEnvironment() else {
        throw ConfigurationError.elasticSearch("💥 missing required arguments")
    }

    services.register(GoogleCloudCredentialsConfiguration())
    services.register(PubSubConfiguration(scope: [.fullControl], serviceAccount: "default"))

    services.register(Store.self) { container -> Store in
        let mysqlDatabaseConnectable = MySQLDatabaseConnectable(container: container)
        let elasticSearchConnectable = ElasticSearchConnectable(container: container, config: elasticSearchConfig)
        let httpClientConnectable = HTTPClientConnectable(container: container)
        let messageQueueClient = try container.make(PublisherClient.self)

        return Store(databaseConnectable: mysqlDatabaseConnectable,
                     elasticSearchConnectable: elasticSearchConnectable,
                     httpClientConnectable: httpClientConnectable,
                     messageQueueClient: messageQueueClient)
    }

    services.register(NotificationServiceRepresentable.self) { container -> NotificationService<Store> in
        let store = try container.make(Store.self)
        let logger = try container.make(Logger.self)
        return NotificationService(store: store, logger: logger)
    }

    services.register(Router.self) { container -> EngineRouter in
        let router = EngineRouter.default()
        try routes(router, container)
        return router
    }

    return (config, services)
}

enum ConfigurationError: Error {
    case database(String)
    case elasticSearch(String)
}

private func contentConfig() -> ContentConfig {
    var config = ContentConfig()

    // json
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()

    encoder.dateEncodingStrategy = .formatted(.iso8601)
    decoder.dateDecodingStrategy = .formatted(.iso8601)

    config.use(encoder: encoder, for: .json)
    config.use(decoder: decoder, for: .json)

    config.use(decoder: URLEncodedFormDecoder(), for: .urlEncodedForm)

    return config
}

private func corsConfiguration() -> CORSMiddleware.Configuration {
    CORSMiddleware.Configuration(
        allowedOrigin: .all,
        allowedMethods: [.GET, .POST, .PUT, .OPTIONS, .DELETE, .PATCH],
        allowedHeaders: [
            .authToken,
            .contentType,
            .userAgent,
            .accessControlAllowOrigin
        ]
    )
}

extension HTTPHeaderName {
    static let authToken = HTTPHeaderName("Auth-Token")
}
