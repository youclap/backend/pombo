import Vapor
import YouClap

final class NotificationHTTPController: RouterController {
    private let service: NotificationServiceRepresentable

    init(service: NotificationServiceRepresentable) {
        self.service = service
    }

    func boot(router: Router) throws {
        let notification = router.grouped("notification")

        notification.post(Notification.self, use: createNotification)

        let notifications = router.grouped("notifications")
        notifications.get("/", use: readNotifications)
    }

    // MARK: - CRUD Methods

    private func readNotifications(_ request: Request) throws -> Future<[Notification]> {

            _ = try request.authenticationID()

            let profileID: Profile.ID = try request.query.get(at: "profileID")

            return service.notifications(for: profileID)

    }

    private func createNotification(_ request: Request, notification: Notification) throws -> Future<Response> {

        _ = try request.authenticationID()

        let profileID: Profile.ID = try request.query.get(at: "profileID")

        return service.createNotification(profileID: profileID, notification: notification)
            .transform(to: HTTPResponse(status: .created).encode(status: .created, for: request))
    }

    // MARK: - Following Methods

    // MARK: - Private Methods

    private func notificationID(from request: Request) throws -> Notification.ID {
        guard let notificationID = try? request.parameters.next(Notification.ID.self) else {
            throw Abort(.badRequest)
        }
        return notificationID
    }

}
