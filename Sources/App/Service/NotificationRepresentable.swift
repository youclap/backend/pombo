import NIO
import protocol Vapor.Service
import YouClap

protocol NotificationServiceRepresentable: Service {
    func notifications(for profileID: Profile.ID) -> EventLoopFuture<[Notification]>
    func createNotification(profileID: Profile.ID, notification: Notification) -> EventLoopFuture<Void>
}
