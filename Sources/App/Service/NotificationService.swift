import NIO
import YouClap

final class NotificationService<Store>: NotificationServiceRepresentable where Store: NotificationStore {

    let store: Store
    let logger: Logger

    init(store: Store, logger: Logger) {
        self.store = store
        self.logger = logger
    }

    func notifications(for profileID: Profile.ID) -> EventLoopFuture<[Notification]> {
        store.notifications(for: profileID)
    }

    func createNotification(profileID: Profile.ID, notification: Notification)
    -> EventLoopFuture<Void> {
        switch notification {
        case let .addedToGroup(addedByProfileID: profileID, groupID: groupID):
            return store.profilesForNotificationAddedToGroup(profileID: profileID, groupID: groupID)
                .flatMap { profileIDs -> [EventLoopFuture<Void>] in
                    self.logger.info("""
                        Notification addedToGroup() is going to affect this profiles: '
                        \(profileIDs.map { String($0) }.joined(separator: ", "))'
                        """)
                    return profileIDs.compactMap { profileID in
                        self.store.createNotificationAddedToGroup(profileID: profileID, groupID: groupID)
                    }

                }
        case let .challengePrivateForGroup(challengeID: challengeID, creatorID: creatorID, groupID: groupID):
            return store.profilesForNotificationAddedToGroup(profileID: profileID, groupID: groupID)
                .flatMap { profileIDs -> [EventLoopFuture<Void>] in
                    self.logger.info("""
                        Notification challengePrivateForGroup() is going to affect this profiles: '
                        \(profileIDs.map { String($0) }.joined(separator: ", "))'
                        """)
                    return profileIDs.compactMap { profileID in
                        self.store.createNotificationChallengePrivateForGroup(profileID: profileID,
                                                                              challengeID: challengeID,
                                                                              creatorID: creatorID,
                                                                              groupID: groupID)
                    }

                }
        case let .challengePrivateForProfile(challengeID: challengeID, creatorID: creatorID):
            return store.createNotificationChallengePrivateForProfile(profileID: profileID,
                                                                      challengeID: challengeID,
                                                                      creatorID: creatorID)
        case let .challengePublic(challengeID: challengeID, creatorID: creatorID):
            return store.profilesForNotificationChallengePublic(profileID: creatorID)
                .flatMap { profileIDs -> [EventLoopFuture<Void>] in
                    self.logger.info("""
                        Notification challengePublic() is going to affect this profiles: '
                        \(profileIDs.map { String($0) }.joined(separator: ", "))'
                        """)
                    return profileIDs.compactMap { profileID in
                        self.store.createNotificationChallengePublic(profileID: profileID,
                                                                     challengeID: challengeID,
                                                                     creatorID: creatorID)
                    }

                }
        case let .notificationFollowing(followerID: followerID):
            return store.createNotificationFollowing(profileID: profileID, followerID: followerID)
        }
    }
}
