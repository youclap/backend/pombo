import Vapor

/// Register your application's routes here.
public func routes(_ router: Router, _ container: Container) throws {
//    try router.register(collection: HealthCheckController())

    let notificationService = try container.make(NotificationServiceRepresentable.self)
    try router.register(collection: NotificationHTTPController(service: notificationService))

}
