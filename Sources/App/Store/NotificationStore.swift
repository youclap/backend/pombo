import Fluent
import Vapor
import YouClap

enum NotificationStoreError: Error {
    case notFoundProfileID(Profile.ID)
    case notFoundNotificationID(Notification.ID)
    case unknownNotificationID(Notification.ID)
    case failedToCreateNotification
}

protocol NotificationStore {
    func notifications(for profileID: Profile.ID) -> EventLoopFuture<[Notification]>

    func createNotificationFollowing(profileID: Profile.ID, followerID: Profile.ID) -> EventLoopFuture<Void>

    func createNotificationAddedToGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
    -> EventLoopFuture<Void>

    func createNotificationChallengePublic(profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID)
    -> EventLoopFuture<Void>

    func createNotificationChallengePrivateForProfile(
    profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID)
    -> EventLoopFuture<Void>

    func createNotificationChallengePrivateForGroup(
    profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID, groupID: UnsignedBigInt)
    -> EventLoopFuture<Void>

    func profilesForNotificationAddedToGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
    -> EventLoopFuture<[Profile.ID]>

    func profilesForNotificationChallengePublic(profileID: Profile.ID)
    -> EventLoopFuture<[Profile.ID]>

    func profilesForNotificationChallengePrivateGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
    -> EventLoopFuture<[Profile.ID]>

}

extension Store: NotificationStore {

    // swiftlint:disable function_body_length
    private func notification(by notificationID: Notification.ID, on connection: MySQLConnection)
    -> EventLoopFuture<Notification> {
        let sql = """
            SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NATG.`addedByProfileID`,
                NATG.`groupID`, NULL as challengeID, NULL as creatorID, NULL AS followerID
            FROM Notification as No
            INNER JOIN NotificationType as NT ON NT.id = No.typeID
            INNER JOIN NotificationAddedToGroup AS NATG ON NATG.notificationID=No.id
            WHERE No.id = ?

            UNION

            SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                NCPFG.groupID as groupID, NCPFG.challengeID, NCPFG.creatorID, NULL AS followerID
            FROM Notification as No
            INNER JOIN NotificationType as NT ON NT.id = No.typeID
            INNER JOIN NotificationChallengePrivateForGroup AS NCPFG ON NCPFG.notificationID=No.id
            WHERE No.id = ?

            UNION

            SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                NULL as groupID, NCPFP.challengeID, NCPFP.creatorID, NULL AS followerID
            FROM Notification as No
            INNER JOIN NotificationType as NT ON NT.id = No.typeID
            INNER JOIN NotificationChallengePrivateForProfile AS NCPFP ON NCPFP.notificationID=No.id
            WHERE No.id = ?

            UNION

            SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                NULL as groupID, NCP.challengeID, NCP.creatorID, NULL AS followerID
            FROM Notification as No
            INNER JOIN NotificationType as NT ON NT.id = No.typeID
            INNER JOIN NotificationChallengePublic AS NCP ON NCP.notificationID=No.id
            WHERE No.id = ?

            UNION

            SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                NULL as groupID, NULL as challengeID, NULL as creatorID, NF.`followerID`
            FROM Notification as No
            INNER JOIN NotificationType as NT ON NT.id = No.typeID
            INNER JOIN NotificationFollowing AS NF ON NF.notificationID=No.id
            WHERE No.id = ?;
        """ // retorna: id, profileID, typeID, createdDate
        return connection.raw(sql)
            .binds([notificationID, notificationID, notificationID, notificationID, notificationID])
            .first(decoding: Notification.self)
            .map {

                guard let data = $0 else {
                    throw NotificationStoreError.unknownNotificationID(notificationID)
                }
                return data
            }
    }

    // swiftlint:disable function_body_length
    func notifications(for profileID: Profile.ID) -> EventLoopFuture<[Notification]> {
        databaseConnectable.connection {
            let sql = """
                SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NATG.`addedByProfileID`,
                    NATG.`groupID`, NULL as challengeID, NULL as creatorID, NULL AS followerID
                FROM Notification as No
                INNER JOIN NotificationType as NT ON NT.id = No.typeID
                INNER JOIN NotificationAddedToGroup AS NATG ON NATG.notificationID=No.id
                WHERE No.profileID = ?

                UNION

                SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                    NCPFG.groupID as groupID, NCPFG.challengeID, NCPFG.creatorID, NULL AS followerID
                FROM Notification as No
                INNER JOIN NotificationType as NT ON NT.id = No.typeID
                INNER JOIN NotificationChallengePrivateForGroup AS NCPFG ON NCPFG.notificationID=No.id
                WHERE No.profileID = ?

                UNION

                SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                    NULL as groupID, NCPFP.challengeID, NCPFP.creatorID, NULL AS followerID
                FROM Notification as No
                INNER JOIN NotificationType as NT ON NT.id = No.typeID
                INNER JOIN NotificationChallengePrivateForProfile AS NCPFP ON NCPFP.notificationID=No.id
                WHERE No.profileID = ?

                UNION

                SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                    NULL as groupID, NCP.challengeID, NCP.creatorID, NULL AS followerID
                FROM Notification as No
                INNER JOIN NotificationType as NT ON NT.id = No.typeID
                INNER JOIN NotificationChallengePublic AS NCP ON NCP.notificationID=No.id
                WHERE No.profileID = ?

                UNION

                SELECT No.id, No.profileID, No.`createdDate`, NT.name as typeID, NULL as addedByProfileID,
                    NULL as groupID, NULL as challengeID, NULL as creatorID, NF.`followerID`
                FROM Notification as No
                INNER JOIN NotificationType as NT ON NT.id = No.typeID
                INNER JOIN NotificationFollowing AS NF ON NF.notificationID=No.id
                WHERE No.profileID = ?;
            """
            return $0.raw(sql)
                .binds([profileID, profileID, profileID, profileID, profileID])
                .all(decoding: Notification.self)
        }
    }

    func createNotificationFollowing(profileID: Profile.ID, followerID: Profile.ID) -> EventLoopFuture<Void> {
        databaseConnectable.transaction { conn in
            let sql = """
                INSERT INTO Notification(profileID, typeID)
                VALUES (?, ?);
            """
            return conn.raw(sql)
                .binds([profileID, NotificationType.following.id])
                .run()
                .flatMap {_ in

                    let sql = """
                        SELECT LAST_INSERT_ID() as value;
                    """

                    return conn.raw(sql)
                        .first(decoding: IntRow<Notification.ID>.self)
                        .flatMap {

                            guard let notificationID = $0?.value else {
                                throw NotificationStoreError.unknownNotificationID(1)
                            }

                            let sql = """
                                INSERT INTO NotificationFollowing(notificationID, followerID)
                                VALUES (?, ?);
                            """

                            return conn.raw(sql)
                            .binds([notificationID, followerID])
                            .run()
                        }
                }
        }
    }

    func createNotificationAddedToGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
        -> EventLoopFuture<Void> {
        databaseConnectable.transaction { conn in
            let sql = """
                INSERT INTO Notification(profileID, typeID)
                VALUES (?, ?);
            """
            return conn.raw(sql)
                .binds([profileID, NotificationType.profileAddedToGroup.id])
                .run()
                .flatMap {_ in

                    let sql = """
                        SELECT LAST_INSERT_ID() as `value`;
                    """

                    return conn.raw(sql)
                    .first(decoding: IntRow<Notification.ID>.self)
                    .flatMap {

                        guard let notificationID = $0?.value else {
                            throw NotificationStoreError.unknownNotificationID(1)
                        }

                        let sql = """
                            INSERT INTO NotificationAddedToGroup(notificationID, addedByProfileID, groupID)
                            VALUES (?, ?, ?);
                        """

                        return conn.raw(sql)
                        .binds([notificationID, profileID, groupID])
                        .run()
                    }
                }
        }
    }

    func createNotificationChallengePublic(profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID)
        -> EventLoopFuture<Void> {
        databaseConnectable.transaction { conn in
            let sql = """
                INSERT INTO Notification(profileID, typeID)
                VALUES (?, ?);
            """
            return conn.raw(sql)
                .binds([profileID, NotificationType.newChallengePublic.id])
                .run()
                .flatMap {_ in

                    let sql = """
                        SELECT LAST_INSERT_ID() as value;
                    """

                    return conn.raw(sql)
                    .first(decoding: IntRow<Notification.ID>.self)
                    .flatMap {

                        guard let notificationID = $0?.value else {
                            throw NotificationStoreError.unknownNotificationID(1)
                        }

                        let sql = """
                            INSERT INTO NotificationChallengePublic(notificationID, challengeID, creatorID)
                            VALUES (?, ?, ?);
                        """

                        return conn.raw(sql)
                        .binds([notificationID, challengeID, creatorID])
                        .run()
                    }
                }
        }
    }

    func createNotificationChallengePrivateForProfile(
        profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID)
        -> EventLoopFuture<Void> {
            databaseConnectable.transaction { conn in
                let sql = """
                    INSERT INTO Notification(profileID, typeID)
                    VALUES (?, ?);
                """
                return conn.raw(sql)
                    .binds([profileID, NotificationType.newChallengePrivateProfile.id])
                    .first()
                    .flatMap { _ in

                        let sql = """
                            SELECT LAST_INSERT_ID() as value;
                        """

                        return conn.raw(sql)
                        .first(decoding: IntRow<Notification.ID>.self)
                        .flatMap {

                            guard let notificationID = $0?.value else {
                                throw NotificationStoreError.unknownNotificationID(1)
                            }

                            let sql = """
                                INSERT INTO NotificationChallengePrivateForProfile(
                                    notificationID, challengeID, creatorID
                                )
                                VALUES (?, ?, ?);
                            """

                            return conn.raw(sql)
                            .binds([notificationID, challengeID, creatorID])
                            .run()
                        }

                    }
            }
        }

    func createNotificationChallengePrivateForGroup(
        profileID: Profile.ID, challengeID: UnsignedBigInt, creatorID: Profile.ID, groupID: UnsignedBigInt)
        -> EventLoopFuture<Void> {
            databaseConnectable.transaction { conn in
                let sql = """
                    INSERT INTO Notification(profileID, typeID)
                    VALUES (?, ?);
                """
                return conn.raw(sql)
                    .binds([profileID, NotificationType.profileAddedToGroup.id])
                    .first()
                    .flatMap { _ in

                        let sql = """
                            SELECT LAST_INSERT_ID() as value;
                        """

                        return conn.raw(sql)
                        .first(decoding: IntRow<Notification.ID>.self)
                        .flatMap {

                            guard let notificationID = $0?.value else {
                                throw NotificationStoreError.unknownNotificationID(1)
                            }

                            let sql = """
                                INSERT INTO NotificationChallengePrivateForGroup(
                                    notificationID, challengeID, creatorID, groupID
                                )
                                VALUES (?, ?, ?, ?);
                            """

                            return conn.raw(sql)
                                .binds([notificationID, challengeID, creatorID, groupID])
                                .run()
                        }

                    }
            }
        }

    func profilesForNotificationAddedToGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
        -> EventLoopFuture<[Profile.ID]> {
        databaseConnectable.connection {
            let sql = """
                SELECT GrPr.profileID as value
                FROM Group_Profile as GrPr
                INNER JOIN Profile as P ON P.id=GrPr.profileID
                WHERE GrPr.groupID = ? AND GrPr.profileID != ?
                    AND GrPr.profileID != ?
                    AND P.deleted=0;
            """
            return $0.raw(sql)
                .binds([groupID, profileID, profileID])
                .all(decoding: IntRow<Profile.ID>.self)
                .map { profileIDs in
                    profileIDs.map { profileID -> Profile.ID in
                        profileID.value
                    }
                }
        }
    }

    func profilesForNotificationChallengePublic(profileID: Profile.ID) -> EventLoopFuture<[Profile.ID]> {
        databaseConnectable.connection {
            let sql = """
                SELECT P.id as value
                FROM Profile as P
                INNER JOIN Follower as F on P.id = F.followerProfileID
                WHERE F.followingProfileID = ?
                  AND P.id != ?
                  AND P.deleted=0
                  AND P.id NOT IN (
                      SELECT BP.blockedProfileID
                      FROM BlockedProfile as BP
                      WHERE BP.profileID = ?
                    );
            """
            return $0.raw(sql)
                .binds([profileID, profileID, profileID])
                .all(decoding: IntRow<Profile.ID>.self)
                .map { profileIDs in
                    profileIDs.map { profileID -> Profile.ID in
                        profileID.value
                    }
                }
        }
    }

    func profilesForNotificationChallengePrivateGroup(profileID: Profile.ID, groupID: UnsignedBigInt)
    -> EventLoopFuture<[Profile.ID]> {
        databaseConnectable.connection {
            let sql = """
                SELECT GrPr.profileID as value
                FROM Group_Profile as GrPr
                INNER JOIN Profile as P ON P.id=GrPr.profileID
                WHERE GrPr.groupID = ? AND GrPr.profileID != ?
                    AND GrPr.profileID != ?
                    AND P.deleted=0;
            """
            return $0.raw(sql)
                .binds([profileID, profileID, profileID])
                .all(decoding: IntRow<Profile.ID>.self)
                .map { profileIDs in
                    profileIDs.map { profileID -> Profile.ID in
                        profileID.value
                    }
                }
        }
    }

} //swiftlint:disable:this file_length
